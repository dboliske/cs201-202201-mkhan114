# Midterm Exam

## Total

83/100

## Break Down

1. Data Types:                  20/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  17/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               2/5
4. Arrays:                      15/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  0/5
5. Objects:                     11/20
    - Variables:                3/5
    - Constructors:             3/5
    - Accessors and Mutators:   3/5
    - toString and equals:      2/5

## Comments

1. Good
2. Works, but not sure why you included the while loop.
3. On the right track, but triangle is inverted.
4. Doesn't check word counts.
5. Instance variables aren't private, non-default constructor and mutator don't validate `age`, and there is no `equals` method.
