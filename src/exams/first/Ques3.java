package exams.first;

import java.util.Scanner;

public class Ques3 {

	public static void main(String[] args) {
	
		Scanner input = new Scanner(System.in);
		
		int height=0;
		
		System.out.println("Please enter the height and width for the triangle: ");
		int x = input.nextInt();
		
		for (int width=0 ; width<x ; width++) {
			int count = 0;
			System.out.print("* ");	
			while (width>count) {
				System.out.println();
				count++;
			}
		}
	}
}