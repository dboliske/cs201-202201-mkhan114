package exams.first;

public class Pet {

		
		String name;
		int age;
		
		//Default constructor
		public Pet() {
			this.name = "Kev";
			this.age = 10;
		}
		
		//Non-default constructor
		public Pet(String name, int age) {
			this.name = name;
			this.age = age;
		}
		//Setters
		public void setName(String name) {
			this.name = name;
		}
		public void setAge(int age) {
			this.age = age;
		}
		
		//Getters
		public String getName() {
			return name;
		}
		public int getAge() {
			return age;
		}
		
		//toString method
		public String toString() {
			return name + age;
		}
	}

