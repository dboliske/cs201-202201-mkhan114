package exams.second;

public class Classroom {

	String building;
	String roomNumber;
	int Seats;
	String finalOutput;
	
	//default constructor
	
	public Classroom() {
		this.building = "Stuart";
		this.roomNumber = "201";
		this.Seats = 80;
	}
	
	//non-default constructor
	
	public Classroom(String building, String roomNumber, int Seats) {
	this.building = building;
	this.roomNumber =  roomNumber;
	this.Seats = Seats;
	}
	
	//setters
	
	public void setBuilding(String building) {
		this.building = building;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public void setSeats(int Seats) {
		this.Seats = Seats;
	}
	
	//getters
	
	public String getBuilding() {
		return building;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public int getSeats() {
		return Seats;
	}
	
	//toString() method
	
	public String toString() {
		
		finalOutput = building + roomNumber + Seats;
		
		return finalOutput;
	}
}
