package exams.second;

public class Rectangle extends Polygon {
	
	double width;
	double height;
	
	//default constructor
	
	public Rectangle(String name) {
		super(name);
	}
	
	//setters
	
	public void setHeight(double height) {
		this.height = height;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	
	//getter
	
	public double getHeight() {
		return height;
	}
	public double getWidth() {
		return width;
	}
	
	//formula methods
	
	public double Area() {
		return height * width;
	}
	
	public double perimeter() {
		return 2.0 * (height + width);
	}
	
	public String toString() {
		return "Rectangle's dimensions: \nHeight: " + height + "\nWidth: " + width;
	}
}
