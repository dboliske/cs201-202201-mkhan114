package exams.second;

public class Circle extends Polygon {
	
	double radius;
	
	//default constructor
	
	public Circle(){
		super(name);
	}
	
	//setter
	
	public void setRadius(double width) {
		width = radius;
	}
	
	//getter
	
	public double getRadius() {
		return radius;
	}
	
	//formula methods
	
	public double Area() {
		return Math.PI * radius * radius;
	}
	
	public double Perimeter() {
		return Math.PI * radius * 2;
	}
	
	public String toString() {
		return "Circle's Radius: " + radius;
	}
}