package exams.second;

public class SelectionSort {
	
	static void selectionSort(String array[], int x) {
		
		for(int i = 0; i < x - 1; i++)
		
		{
			int min_index = i;
			String minStr = array[i]; //finding the minimum element in array
			for(int j = i + 1; j < x; j++) {
				
				if(array[j].compareTo(minStr) < 0) //comparing if array[j] is less than minStr
				{
					minStr = array[j];
					min_index = j;
				}
			}
			//swapping the minimum element and the first element
			if(min_index != i) {
				String temp = array[min_index];
				array[min_index] = array[i];
				array[i] = temp;
			}
		}
	}

	public static void main(String[] args) {
		
		String array[] = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality",
				"percentage", "height", "wealth", "resource", "lake", "importance"};

		int x = array.length;
		System.out.println("Given Array: ");
		
		for (int i = 0; i < x; i++) { 
			System.out.println(i +": " + array[i]); //printing given array
		}
		System.out.println();
		
		selectionSort(array, x); //running selectionSort method
		
		System.out.println("\n\nSorted Array: ");
		
		for (int i = 0; i < x; i++) {
			System.out.println(i +": " + array[i]); //printing sorted array
		}
	}
}
