package exams.second;

public class ComputerLab extends Classroom {
	
	boolean computers;
	
	public ComputerLab(String building, String roomNumber, int Seats) {
		//default constructor
		super(building, roomNumber, Seats);
	}
	
	//setter
	
	
	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	//getter
	
	public boolean getComputers() {
		return computers;
	}
	
	public String toString() {
		return finalOutput + computers;
	}
}
