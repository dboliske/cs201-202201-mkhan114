package exams.second;

import java.util.*;

public class JumpSearch {
	
	public static int jumpSearch(double[] array, double x) {
		
		double y = array.length;
		
		double jump = (int)Math.floor(Math.sqrt(y));
		
		double prev = 0;
		
		while (array[(int) (Math.min(jump, y)-1)] < x)
		{
			prev = jump;
			jump += (int)Math.floor(Math.sqrt(y));
			if (prev >= y) 
			return -1;
		}
		
		while (array[(int) prev] < x)
		{
			prev++;
			
			if (prev == Math.min(jump,y))
				return -1;
		}
		
		if (array[(int) prev] == x)
			return (int) prev;
		
		return -1;
	}

	public static void main(String[] args) {
		
		double array[] = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		double x;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please search for a number: ");
		x = scan.nextInt();
		
		double index = jumpSearch(array, x);
		
		System.out.println(x + " is located at " + index);
		
		scan.close();
	}

}
