package exams.second;

import java.util.*;

public class ArrayLists {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		//declaring the ArrayList
		ArrayList<Integer> list = new ArrayList<>();
		int n = list.size();
		int min = 100, max = 0, i;
		
		while (true) {
			System.out.println("Enter a value: ");
			list.add(scan.nextInt());
			
			for(i = 1; i < n; i++ ) {
				if (i < min) {
					min = i;
				}
				else if (i > max) {
					max = i;
				}
			}
		break;
		}
		System.out.println("Max: " + max);
		System.out.println("Min: " + min);
	}
}