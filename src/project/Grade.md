# Final Project

## Total

132/150

## Break Down

Phase 1:                                                            32/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            1/3
  - Describe how you will process the data from the input file      0/4
  - Describe how you will store the data                            1/3
  - How will you add/delete/modify data?                            5/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           4/7
- UML Class Diagrams                                                2/10
- Testing Plan                                                      7/10

Phase 2:                                                            100/100

- Compiles and runs with no run-time errors                         10/10
- Documentation                                                     15/15
- Test plan                                                         10/10
- Inheritance relationship                                          5/5
- Association relationship                                          5/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           5/5
- Project generates paths between any two stations                  10/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            10/10
- All classes are complete (getters, setters, toString, equals...)  5/5

## Comments

### Design Comments
2a : Did not explain how you would read the csv file -2
2b : Did not explain how you would process data -3
2c : Did not explain details about how you would store the data from the input file -2
Classes : README.md file stated that you need to have 3 types of objects and you have 1/3 -3
Missing item in UML diagram -8
Testing Plan : Your testing plan doesn't have specific input and outputs and has some functions are missing -3
### Code Comments
