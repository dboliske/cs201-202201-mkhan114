package project;
public class ShoppingCartItem {
    String itemName;
    double itemPrice;
    String expAgeLimit;
    public Object quantity;

public ShoppingCartItem(){
    itemName = "Default";
    itemPrice = 20;
    expAgeLimit = "No Expiration or age limit required";
    }

    public ShoppingCartItem(String itemName, double itemPrice, String expAgeLimit){
        this.itemName = itemName;
        this.expAgeLimit = expAgeLimit;
        this. itemPrice = itemPrice;
    }

    public String getItemName(){
        return itemName;
    }

    public double getItemPrice(){
        return itemPrice;
    }

    public void setItemName(String itemName){
        this.itemName = itemName;
    }
    
    public void setItemPrice(double itemPrice){
        this.itemPrice = itemPrice;
    }

    public void setExpAgeLimit(String expAgeLimit){
        this.expAgeLimit = expAgeLimit;
    }

    //Creating duplicates of the item
    public ShoppingCartItem copy(){
        return new ShoppingCartItem(itemName, itemPrice, expAgeLimit);
    }

    public String toString(){
        String product = "Name: " + itemName;
        product += "\nPrice: " + super.toString();
        return product;
        }

    public void subtract(ShoppingCartItem oldItem) {
    }

    public void add(ShoppingCartItem newItem) {
    }
    }