package labs.lab7;

public class BubbleSort {
	
	public static void bubbleSort(int[] arr)
	{
		int n = arr.length; //length of array passed to n
		int temp = 0; //temporary variable
		  
		for(int i = 0; i < n; i++) { //loop to run in every pass
		for(int j=1; j < (n-i); j++) { //loop for passes
		if(arr[j-1] > arr[j]) { //compares the adjacent elements
		temp = arr[j-1]; //swap if above condition satisfy   
		arr[j-1] = arr[j];
		arr[j] = temp;
		}
	  }
   }
}
	
		public static void main(String[] args) {
		int arr[] = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9 };
		System.out.println("Array Before Bubble Sort\n");

		for(int i = 0; i < arr.length; i++) { // before sorting
		System.out.print(arr[i] + " ");
	}
		System.out.println("\n");
		bubbleSort(arr); //running method
		System.out.println("Array After Bubble Sort\n");

		for(int i = 0; i < arr.length; i++) { // after sorting
			
		System.out.print(arr[i] + " ");
		}
	}
}
