package labs.lab7;

public class SelectionSort {
	
	void sort(double arr[]) //SelectionSort method
	{
		int n = arr.length;
		
		for (int i = 0; i < n-1; i++)
		{
			
			int min_idx = i;
			for (int j = i+1; j < n; j++)
				if (arr[j] < arr[min_idx])
					min_idx = j;
			
			double temp = arr[min_idx];
			arr[min_idx] = arr[i];
			arr[i] = temp;
		}
	}

	void printArray(double arr[]) //Printing method
	{
		int n = arr.length;
		for (int i = 0; i < n; ++i)
			System.out.print(arr[i]+"  ");
	}
	
	public static void main(String[] args) {
		
		SelectionSort ob = new SelectionSort();
		double arr[] = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		ob.sort(arr); //running sorting method
		System.out.println("Sorted Array: \n");
		ob.printArray(arr); //running print method

	}

}
