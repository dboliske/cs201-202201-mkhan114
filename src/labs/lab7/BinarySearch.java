package labs.lab7;

	public class BinarySearch {
		    
		    
    // Returns index of x if it is present in arr[l..r], else return -1
	   public static int binarySearch(String arr[], int l, int r, String x) {
		 if (r >= l) {
			 int mid = l + (r - l) / 2;

     // If the element is present at the middle itself
		   if (arr[mid].equals(x))
		     return mid;

	 // If element is smaller than mid, then it can only
     // be present in left sub-array
		   
		 if (arr[mid].compareTo(x) > 0)
		  return binarySearch(arr, l, mid - 1, x);

	 // Else the element can only be present in right sub-array
		  return binarySearch(arr, mid + 1, r, x);
		        }

	 // We reach here when element is not present in array
		  return -1;
		    }

	// Driver method to test above
		 public static void main(String args[]) {

		    String array[] = {"c", "html", "java", "python", "ruby", "scala"};
		      
		      int n = array.length;
		      String searchString = "html";
		      int result = binarySearch(array, 0, n - 1, searchString);
		      if (result == -1)
		         System.out.println("Element not found");
		      else
		         System.out.println("Element found at index " + result);
	}
}
