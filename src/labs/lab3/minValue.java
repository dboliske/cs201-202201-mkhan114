package labs.lab3;
public class minValue {
  public static int Min(int arr[])
  {
        int result = arr[0];
        for(int i = 1; i < arr.length; i++)
            result = Math.min(result, arr[i]);
        return result;
  }
  public static void main(String[] args) {

int array[] ={72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
    System.out.println("Minimum Element in the array is "+ Min(array));
  }
}