package labs.lab3;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.File;

public class array {

public static void main(String[] args) throws IOException {
    
    Scanner sc = new Scanner(System.in);
    String Contents = "";
    while (true) {
        System.out.println("Enter the Number. For Terminating Please Enter 'Done': ");
        String Input = sc.nextLine();
        if ("Done".equalsIgnoreCase(Input)) {
          break;
        }
        Contents += Input + " ";
    }
    System.out.println("Content to be stored in file " + Contents);
    System.out.print("Enter the Filename: ");
    String fileName = sc.nextLine();
    System.out.println("fileName: " + fileName);
    File file = new File(fileName);
    file.createNewFile();
 
    FileWriter myWriter = new FileWriter(fileName);
    myWriter.write(Contents);
    myWriter.close();
  }
}