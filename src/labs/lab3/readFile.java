package labs.lab3;

import java.io.BufferedReader;

import java.io.FileReader;

import java.io.IOException;

public class readFile {

public static void main(String[] args) {

String line = "";

String splitBy = ",";

try {

// parsing a CSV file into BufferedReader class constructor

try (BufferedReader br = new BufferedReader(new FileReader("src/labs.lab3/grades.csv"))) {
	float sum=0 ;
	
	int count=0;
	
	while ((line = br.readLine()) != null) // returns a Boolean value
	
	{
	
	String[] students = line.split(splitBy); // use comma as separator
	
	int marks = Integer.parseInt(students[1]); // get marks
	
	sum += marks; //calculates sum of marks
	
	count++; //count of total students
	
	}
	
	float avg = sum/count; //gives average
	
	System.out.println("Average grade = " + avg); //prints average
} catch (NumberFormatException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
  }

} catch (IOException e) {

    e.printStackTrace();

  }

}

}