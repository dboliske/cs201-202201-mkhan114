package labs.lab1;

import java.util.Scanner;

public class tempProblem {
	
	public static void main(String[] args) {
		
		double inputFahren; //Var for fahrenheit
		double inputCelc; // Var for celcius
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter a value for fahrenheit: ");
		inputFahren = input.nextDouble(); //Inputting value for fahrenheit
		
		System.out.println("Please enter a value for celcius: ");
		inputCelc = input.nextDouble(); //inputting value for celsius to be converted
		
		double convFahren = ((inputFahren - 32)*5/9); //formula to convert entered fahrenheit value to celsius
		
		double convCelc = ((inputCelc*1.8)+32); //formula to convert entered celsius value to fahrenheit
		
		System.out.println("Your entered value for fahrenheit converted to celcius is " + convFahren);
		
		System.out.println("Your entered value for celsius converted to fahrenheit is " + convCelc);
		
	}

}

//The results from our various sample values were accurate