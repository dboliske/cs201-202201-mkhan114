package labs.lab1;

import java.util.Scanner;

public class NameInput {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		String name; //Declaring var to store name in 
		
		System.out.println("Please enter your name: ");
		name = input.nextLine(); //Storing user's input in string variable
		
		System.out.println("The name entered is: "+ name);
		
	}
		

}
