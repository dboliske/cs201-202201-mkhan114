package labs.lab1;

import java.util.Scanner;

public class woodBox {

	public static void main(String[] args) {
		
		double length;
		double width;
		double depth;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Length: ");
		length = input.nextDouble(); //Inputting value for length
		
		System.out.println("Width: ");
		width = input.nextDouble(); //Inputting value for width
		
		System.out.println("Depth: ");
		depth = input.nextDouble(); //Inputtiung value for depth
		
		double area = (length*depth)*2+(width*depth)*2+(length*width)*2; //Formula to get the area of the box
		
		System.out.println("The amount of wood needed to build the box: " + area + " sq. ft");

	}

}

//Results recorded were accurate to what the anticipated answers were.