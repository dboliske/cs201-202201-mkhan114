package labs.lab1;

import java.util.Scanner;

public class inchesToCms {

	public static void main(String[] args) {
		
		double inches; // Var for storing value in inches
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter a value to be converted from inches to centimeters: ");
		inches = input.nextDouble();
		
		double centiMeters = inches*2.54; // Formula for converting inches to centimeters
		
		System.out.println("The value entered in centimeters is: " + centiMeters);

	}

}
