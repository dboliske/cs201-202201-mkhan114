package labs.lab1;

import java.util.Scanner;

public class charProblem {

	public static void main(String[] args) {
		
		String initial;
		
		Scanner input = new Scanner(System.in); //Declaring a scanner object
		
		System.out.println("Please enter your first name: ");
		
		initial = input.nextLine(); //Storing name in string variable "initial"
		
		System.out.println("First initial of your name: " + initial.charAt(0)); //Printing out just the first letter of the name
		
		

	}

}
