package labs.lab1;

public class Arithmetic {

	public static void main(String[] args) {
		
		int Dad;  //Declaring variable to store dad's age
		int Self; //Declaring one for myself
		int Birth; //Variable to store my birth year
	    int heightInches; //Var for height in inches
		
		Dad = 53;  
		Self = 20;
		
		int Diff;
		
		Diff = Dad - Self; //Subtracting both ages to store in Diff var
		
		System.out.println("The difference in age between my father and I: "+ Diff);
		
		Birth = 2002;
		
		int doubleBirth = 2*Birth; //Multiplying birth year by 2 
		
		System.out.println("My birth year multiplied by two: "+ doubleBirth);
		
		heightInches = 69; //My height
		
		float heightCms = heightInches*(float)2.54; //Converting height from inches to centimeters
		
		System.out.println("My height is 69 inches, and to convert that into cm, it would be multiplied by 2.54: "+ heightCms);
		
		
		int feet = heightInches/12; //Declaring value for feet in height
		
		int inches = heightInches%12; //Declaring value for inches which would be the remainder of height by 12
		
		System.out.println("Height in feet and inches: "+ feet + "'" + inches + "\"");

	}

}
