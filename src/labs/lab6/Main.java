package labs.lab6;
import java.util.ArrayList;
import java.util.Scanner;

//importing java utility class

public class Main {

	//initializing scanner class as static variable so it can be user everywhere
	static Scanner sc = new Scanner(System.in);
	
	public int addCustomer(ArrayList<String> customerQueue) {
		//taking name for the customer
		System.out.print("Enter name of the customer: ");
		
		//extra sc.nextLine() is important to break the sc.nextInt()
		sc.nextLine();
		
		//taking input for the name of the customer
		String name = sc.nextLine();
		
		customerQueue.add(name);
		
		return customerQueue.size();
	}
	
		public void helpCustomer(ArrayList<String> customerQueue) {
			
			String customerName = customerQueue.remove(0);
			
			System.out.println(customerName);
		}
			
		public void menu(ArrayList<String> customerQueue) {
			
			//initializing choice with 0
			int choice = 0;
			
			//while choice is not 3
			while(choice != 3) {
				//printing menu
				System.out.println("1. Add customer to queue");
				System.out.println("2. Hep customer");
				System.out.println("3. Exit");
				System.out.println("Enter your choice: ");
				
				choice = sc.nextInt();
				
				//if choice is 1 call addCustomer method
				if(choice == 1) {
					addCustomer(customerQueue);
				}
				//if choice is 2 call helpCustomer
				else if(choice == 2) {
					helpCustomer(customerQueue);
				}
				//if choice is anything besides 1, 2, or 3 print invalid input
				else if(choice != 3) {
					System.out.println("Enter a valid input");
				}
			}
	}
		
		public static void main(String[] args) {
			//declaring ArrayList
			ArrayList<String> customerQueue = new ArrayList<String>();
			
			Main deli = new Main();
			
			//calling menu method
			deli.menu(customerQueue);
		}
}
