//Instructions for square: I will declare two variables which will print out asterisks in a line until
//they reach their max value through a "for" loop.  Each variable will be used for each line of asterisks
//printed.

package labs.lab0;

public class Square {

	public static void main(String[] args) {
		
		for(int x=0 ; x<2 ; x++) {
			System.out.print("*");
		}
		System.out.println();
		
		for(int y=0; y<2 ; y++) {
			System.out.print("*");
		}

	}

}
//I expected to be able to do this in a single for loop but I had to use a separate one for each variable
//The result is exactly what I wanted, however.