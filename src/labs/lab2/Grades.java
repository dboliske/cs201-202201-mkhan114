package labs.lab2;

import java.util.Scanner;

public class Grades {

	public static void main(String[] args) {
		
		        Scanner input = new Scanner(System.in); //initiating scanner
		        System.out.print("Enter Scores ('-1' signifies end):");
		        int scores[] = new int[50];
		        int numberofScores = 0; //variable to set amount of values
		        int average = 0; //variable to get average 
		        for (int i = 0; i < 50; i++) {
		            scores[i] = input.nextInt();
		            if (scores[i] < 0)
		                break;
		            average += scores[i];
		            numberofScores++;
		        }
		        average /= numberofScores;
		       
		        System.out.println("\n Average of scores:" + average);
		        
	}
}
