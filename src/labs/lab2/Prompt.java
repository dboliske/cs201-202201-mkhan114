package labs.lab2;

import java.util.Scanner;

public class Prompt {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean done = false; //boolean value to keep us in a while loop to keep user returning to the menu
		int choice = 0; //variable to store the user input for their choice
		
		
		while (!done) { //while loop to keep user returning to the menu
			System.out.println();
			System.out.println("Please choose a number from 1-4: ");
			System.out.println("1. Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			System.out.println();
			choice = input.nextInt();
			
			switch(choice) {
			case 1:  //option 1 
			System.out.println("Hello!");
		break;
			case 2:  //option 2
			System.out.println("Please enter a value to be added");
			int x = input.nextInt();
			System.out.println("Please enter a second value: ");
			int y = input.nextInt();
			int z = x+y; //adding two values entered by the user
			System.out.println("The sum of the two values: " + z);
		break;
		
			case 3:  //option 3 
			System.out.println("Please enter a value to be multiplied: ");
			int x1 = input.nextInt();
			System.out.println("Please enter a second value: ");
			int y1 = input.nextInt();
			int z1 = x1*y1; //multiplying the two values entered by the user
			System.out.println("The product of the values: " + z1);
		break;
		
			case 4: 
				done = true; //boolean equals true meaning while loop ends
				System.out.println("You've exited");
           break;
			}
			System.out.println();
			System.out.println("Thank you!");
			}
		}
	}