package labs.lab2;

import java.util.Scanner;

public class Square {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int dimension; //variable to be used to store user entered value for box dimensions
		
		System.out.println("Please enter a value for the dimensions of the square: ");
		dimension = input.nextInt();
		
		for (int x = 0; x < dimension; x++) { //first 'for' loop to be used in order to move vertically
			for (int y = 0; y < dimension; y++) { //this loop and variable will print '*' horizontally until 'y' equals user entered value
				System.out.print("* ");
			}
			System.out.println(); //skipping line for height dimensions
		}
		
	}

}