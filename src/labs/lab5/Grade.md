# Lab 5

## Total
17/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 1/1
- Accessors:                    2/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                2/2
- Loops to display menu:        2/2
- Displays station names:       0/1
- Displays stations by access:  0/2
- Displays nearest station:     2/2
- Exits                         1/1

## Comments
There's a lot code here that wasn't required of you. You have some missing options for your menu loop as well such as displayStationNames and displayByWheelchair.

Remember to use your default super() constructor in your default constructor for CTAStaton as well. 

Most of your coding is correct, however there were some errors that showed up due to the program not being able to read the file. I didn't take points off for that but points were taken off for missing parts of the lab that were required




