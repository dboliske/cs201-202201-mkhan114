package labs.lab5;
import java.util.ArrayList;
import java.util.Arrays;


public class CTAStation extends GeoLocation {
	
	private String name, location;
	private ArrayList<String[]> lineColors;
	private boolean wheelChair;
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelChair, ArrayList<String[]> color) {
		super(lat,lng);
		this.name = name;
		this.location = location;
		this.lineColors = color;
		this.wheelChair = wheelChair;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public ArrayList<String[]> getLineColors() {
		return lineColors;
	}

	public void setLineColors(ArrayList<String[]> lineColors) {
		this.lineColors = lineColors;
	}

	public boolean isWheelChair() {
		return wheelChair;
	}

	public void setWheelChair(boolean wheelChair) {
		this.wheelChair = wheelChair;
	}
	
	public boolean equals(CTAStation obj) {
		if(!obj.getName().equalsIgnoreCase(name)) {
			return false;
		}
		if(obj.getLat() != super.getLat()) {
			return false;
		}
		if(obj.getLng() != super.getLng()) {
			return false;
		}
		if(!obj.location.equalsIgnoreCase(location)) {
			return false;
		}
		if(obj.wheelChair != wheelChair) {
			return false;
		}
		if(obj.getLineColors() != lineColors) {
			return false;
		}
		return true;
	}

	public String toString() {
		String stationList = "";
		for(int i = 0; i < lineColors.size();i++) {
			stationList += "\n" + lineColors.get(i)[0];
		}
		return "Name: "+ name + "\nLat & Long:( "+ super.getLat() +", "+ super.getLng()+")\nLocation: "+location +"\nWheelChair: "+ wheelChair +"\nStationColors: "+ stationList;
	}

}
