package labs.lab5;

public class GeoLocation {
	
private double lat,lng;
	
	public GeoLocation() {
		lat = 0.0;
		lng = 0.0;
	}
	
	public GeoLocation(double nlat, double nlng) {
		lat = nlat;
		lng = nlng;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double nlat) {
		lat = nlat;
	}
	
	public void setLng(double nlng) {
		lng = nlng;
	}
	public boolean isLatBetween() {
		if(lat > -90 && lat<90) {
			return true;
		}
		return false;
	}
	
	public boolean isLngBetween() {
		if(lng > -180 && lng < 180) {
			return true;
		}
		return false;
	}
	
	public double calcDistance(GeoLocation obj) {
		return Math.sqrt(Math.pow(obj.getLat()-getLat(), 2)+Math.pow(obj.getLng()-getLng(), 2));
		
	}
	
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(lat-getLat(), 2)+Math.pow(lng-getLng(), 2));
	}
	public String convertToLocatoin(String str) {
		double tempLat = Double.parseDouble(str.substring(1,str.indexOf(",")));
		double tempLng = Double.parseDouble(str.substring(str.indexOf(",")+1,str.length()-1));
		return "Lat: "+tempLat+"\nLong: "+tempLng;
	}
	public String toString() {
		return "("+lat+", "+lng+")";
	}
}
