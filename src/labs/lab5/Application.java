package labs.lab5;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Application {
	
	private static int numRed = 33, numGreen=28, numBlue=33, numBrown=27, numPurple=27, numPink=16, numOrange=16, numYellow=3; 
	private static ArrayList<CTAStation> stations;
	//Reads the CTAStop file and then runs the menu() method
	public static void main(String[] args) {
		stations = new ArrayList<CTAStation>();
		readFile();
		menu();
		
	}
	//menu() method creates a method loop that keeps going and asks the user to pick the options
	public static void menu() {
		boolean loop = true;
		Scanner input = new Scanner(System.in);
		boolean isLoop = true;
		while(loop) {
			do {
				System.out.println("Please select an option:\n1.Add Station\n2.Edit A Station\n3.Remove A Station\n4.Search for Station\n5.Find nearest station\n6.Create a route\n7.Exit ");
				int value = Integer.parseInt(input.nextLine());
				switch(value) {
					case 1:
						addStation();
						break;
					case 2:
						editStation();
						break;
					case 3: 
						removeStation();
						break;
					case 4:
						searchStation();
						break;
					case 5:
						findNearestStation();
						break;
					case 6:
						//createRoute();
						break;
					case 7:
						loop = false;
						break;
					default:
						System.out.println("Invalid input, please try again");
						isLoop = false;
						break;
						
				}
			} while(!isLoop);
		}
		
		
	}
	//This method handles the creation of a route the user defines (Doesn't work)
	public static void createRoute() {
		Scanner input = new Scanner(System.in);
		System.out.println("What do you want to name your route?");
		String name = input.nextLine();
		System.out.println("What station are you at?");
		CTAStation firstStation = searchStation(input.nextLine());
		System.out.println("What station do you want to go to?");
		CTAStation lastStation = searchStation(input.nextLine());
		boolean isOnSameLine = false;

		for(int i = 0; i < (firstStation.getLineColors().size()+lastStation.getLineColors().size())-1;i++) {
			String[] firstColors = firstStation.getLineColors().get(i);
			String[] lastColors = lastStation.getLineColors().get(i);
			System.out.println(Arrays.deepToString(firstColors));
			System.out.println(Arrays.deepToString(lastColors));
			if(firstColors[0].equalsIgnoreCase(lastColors[0])) {
				for(int j = 0; i < stations.size();j++) {
					int firstStationIndex = Integer.parseInt(firstColors[1]);
					int lastStationIndex = Integer.parseInt(lastColors[1]);
					System.out.println(Math.abs(firstStationIndex-lastStationIndex));
				}
			}
		}
		
		
		
	}
	
	private static CTAStation searchStation(String nextLine) {
		// TODO Auto-generated method stub
		return null;
	}
	//This method asks the location of the user and returns the closest station
	public static void findNearestStation() {
		Scanner input = new Scanner(System.in);
		System.out.println("What is the latitude of the station?");
		double lat = Double.parseDouble(input.nextLine());
		System.out.println("What is the longtitude of the station?");
		double lng = Double.parseDouble(input.nextLine());
		CTAStation tempObj = stations.get(0);
		double tempDistance = stations.get(0).calcDistance(lat,lng);
		for(int i = 0; i < stations.size()-1; i++) {
				if(tempDistance >= stations.get(i+1).calcDistance(lat, lng)) {
					tempDistance = stations.get(i+1).calcDistance(lat, lng);
					tempObj = stations.get(i+1);
				}
		}
		
		System.out.println("Closest Station:\n" + tempObj);
	}

	//This method looks through all the stations in the arraylist and returns the one you're looking for.
	public static void searchStation() {
		Scanner input = new Scanner(System.in);
		System.out.println("What is the name of the station you're looking for?");
		String name = input.nextLine();
		for(int i = 0; i < stations.size(); i++) {
			if(stations.get(i).getName().equals(name)) {
				System.out.println(stations.get(i));
				System.out.println();
				
			}
		}
		
	}
	
	
	//This method removes the station you ask for from the current list
	public static void removeStation() {
		Scanner input = new Scanner(System.in);
		System.out.println("What is the name of the station you want to delete?");
		String name = input.nextLine();
		int stationIndex = 0;
		for(int i = 0; i < stations.size(); i++) {
			if(stations.get(i).getName().equals(name)) {
				stationIndex = i;
				
			}
		}
		
		stations.remove(stationIndex);
	}
	//This method edits the station you request for
	public static void editStation() {
		
	}
	
	//This method adds a station and adds it to the stations array.
	public static void addStation() {
		Scanner input = new Scanner(System.in);
		System.out.println("What do you want to name your station?");
		String name = input.nextLine();
		System.out.println("What is the latitude of your station?");
		double lat = Double.parseDouble(input.nextLine());
		System.out.println("What is the longitude of your station?");
		double lng = Double.parseDouble(input.nextLine());
		System.out.println("What is the location of your station? i.e Elevated, Subway, Embankment");
		String location = input.nextLine();
		System.out.println("Is it wheelchair acessible?(y or n)");
		boolean wheelChair = false;
		if(input.nextLine().equalsIgnoreCase("y")) {
			wheelChair = true;
		}
		
		ArrayList<String[]> tempArray = new ArrayList<String[]>();
		boolean isLoop = true;
		do {
			String color = "",index = "";
			System.out.println("What line(s) go through your station?\n1.Red\n2.Green\n3.Blue\n4.Brown\n5.Purple\n6.Pink\n7.Orange\n8.Yellow");
			int value = Integer.parseInt(input.nextLine());
			switch(value) {
				case 1:
					color = "Red";
					index = ""+numRed++;
					break;
				case 2:
					color ="Green";
					index = ""+numGreen++;
					break;
				case 3:
					color = "Blue";
					index = ""+numBlue++;
					break;
				case 4:
					color = "Brown";
					index = ""+numBrown++;
					break;
				case 5:
					color = "Purple";
					index = ""+numPurple++;
					break;
				case 6:
					color = "Pink";
					index = ""+numPink++;
					break;
				case 7:
					color = "Orange";
					index = ""+numOrange++;
					break;
				case 8:
					color = "Yellow";
					index = ""+numYellow++;
					break;
			}
			String[] colorArr = new String[] {color, index};
			tempArray.add(colorArr);
			System.out.println("Would you like to add another line?(y or n)");
			if(input.nextLine().equalsIgnoreCase("y")) {
				isLoop = false;
			}else {
				isLoop = true;
			}
		}while(!isLoop);
		
		stations.add(new CTAStation(name, lat, lng, location, wheelChair, tempArray));
		
		
	}
	//This method reads the file and creates Station objects and stores it in the stations array.
	public static void readFile(){
		try{
			
			File file = new File("src/project/CTAStops.csv");
			Scanner read = new Scanner(file);
			read.nextLine();
			read.nextLine();
			
			for(int i = 0; read.hasNextLine();i++) {
				String ln = read.nextLine();
				String split[] = ln.split(",",0);
				String name = split[0];
				double lat = Double.parseDouble(split[1]);
				double lng = Double.parseDouble(split[2]);
				String location = split[3];
				boolean wheelChair = Boolean.parseBoolean(split[4]);
				int red = Integer.parseInt(split[5]);
				int green = Integer.parseInt(split[6]);
				int blue = Integer.parseInt(split[7]);
				int brown = Integer.parseInt(split[8]);
				int purple = Integer.parseInt(split[9]);
				int pink = Integer.parseInt(split[10]);
				int orange = Integer.parseInt(split[11]);
				int yellow = Integer.parseInt(split[12]); 
				ArrayList<String[]> color = calcColor(red,green,blue,brown,purple,pink,orange,yellow);
				stations.add(new CTAStation(name,lat,lng,location,wheelChair,color));
			}
			
		}catch (Exception e){
			System.out.println("Error, file failed to read.");
		}
	}
	//Returns an array that takes in the value of the colors and gives a string for color and a string for its index
	public static ArrayList<String[]> calcColor(int red, int green, int blue, int brown, int purple, int pink, int orange, int yellow) {
		ArrayList<String[]> stationColors = new ArrayList<String[]>();

			
			if(red >=0) {
				String[] color = new String[2];
				color[0] = "Red";
				color[1] = ""+red;
				stationColors.add(color);
			}
			
			if(green >=0) {
				String[] color = new String[2];
				color[0] = "Green";
				color[1] = ""+green;
				stationColors.add(color);
			}
			if(blue >=0) {
				String[] color = new String[2];
				color[0] = "Blue";
				color[1] = ""+blue;
				stationColors.add(color);
			}
			if(brown >=0) {
				String[] color = new String[2];
				color[0] = "Brown";
				color[1] = ""+brown;
				stationColors.add(color);
			}
			if(purple >=0) {
				String[] color = new String[2];
				color[0] = "Purple";
				color[1] = ""+purple;
				stationColors.add(color);
			}
			if(pink >=0) {
				String[] color = new String[2];
				color[0] = "Pink";
				color[1] = ""+pink;
				stationColors.add(color);
			}
			if(orange >=0) {
				String[] color = new String[2];
				color[0] = "Orange";
				color[1] = ""+orange;
				stationColors.add(color);
			}
			if(yellow >=0) {
				String[] color = new String[2];
				color[0] = "Yellow";
				color[1] = ""+yellow;
				stationColors.add(color);
			}
		
		
		return stationColors;
	}
	//Prints out the stations in the current array
	public static void listStations() {
		for(int i = 0; i < stations.size(); i++) {
			System.out.println(stations.get(i).getName());
		}
		
	}

}
