package labs.lab4;


public class PhoneApp {

	public static void main(String[] args) {
		
		
		
		//Setting values using default constructor
		
		PhoneNumber Phone1 = new PhoneNumber();
		
		//Printing the number using the default constructor
		
		System.out.println(Phone1.toString());
		
		//Setting values for non-default constructor
		String areaCode = "713";
		String countryCode = "1";
		String number = "2817481";
		
		//Declaring a second phone object to utilize the non-default constructors
		PhoneNumber Phone2 = new PhoneNumber(areaCode, countryCode, number);
		
		System.out.println();
		
		//Printing number using non-default constructor
		System.out.println(Phone2.toString());
		
	}

}
