package labs.lab4;

public class Potion {
	
	String name;
	double strength;

	//Default constructor
	public Potion() {
		this.name = "Elixir";
		this.strength = 10;
	}
	
	//Non-default constructor
	public Potion(String name, double strength) {
		this.name = name;
		this.strength = strength;
	}
	
	//Getters
	public String getName() {
		return name;
	}
	public double getStrength() {
		return strength;
	}
	
	//Setters
	
	public void setName(String name) {
		this.name = name;
	}
	public void setStrength(double strength) {
		this.strength = strength;
	}
	
	//Method for toString
	
	public String toString() {
		return name + ":" + " " + strength + "mg";
	}
	
	public boolean CheckStrength() {
		if (strength>0 && strength<10)
			return true;
		return false;
	}
	
}