package labs.lab4;

public class GeoLocation {
	
		   private double lat;
		   private double lng;
		  
		   //Default constructor
		   GeoLocation() {
		       super();
		   }
		  
		   //non-default constructor which takes lat and lng as arguments.
		   public GeoLocation(double lat, double lng) {
		       super();
		       this.lat = lat;
		       this.lng = lng;
		   }

		   //getter method for lat
		   public double getLat() {
		       return lat;
		   }
		   //setter method for lat
		   public void setLat(double lat) {
		       this.lat = lat;
		   }
		  
		   //getter method for lng
		   public double getLng() {
		       return lng;
		   }
		  
		   //setter method for lng
		   public void setLng(double lng) {
		       this.lng = lng;
		   }
		  
		   //toString() method . It is used to print the object in desirec format
		   public String toString() {
		       return "("+lat + "," + lng + ")";
		   }
		  
		   //checks lat whether it is between -90 and 90.8 and return true it it is.
		   public boolean checkLat()
		   {
		       if(this.lat>=-90 && this.lat<=90.8)
		           return true;
		       return false;
		   }
		  
		   //checks lng whether it is between -180 and 180.9 and return true it it is.
		   public boolean checkLong()
		   {
		       if(this.lng>=-180 && this.lng<=180.9)
		           return true;
		       return false;
		   }
		  

		}
