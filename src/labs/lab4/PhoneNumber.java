package labs.lab4;

public class PhoneNumber {

	private String countryCode;
	private String areaCode;
	private String number;
	
	//Default constructor
	public PhoneNumber() {
		this.areaCode = "708";
		this.countryCode = "1";
		this.number = "8432735";
	}
	
	//Non-default constructor
	public PhoneNumber(String areaCode, String countryCode, String number) {
		this.areaCode = areaCode;
		this.countryCode = countryCode;
		this.number = number;
	}
	
	//Getters
	
	public String getAreaCode() {
		return areaCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public String getNumber() {
		return number;
	}
	
	//Setters
	
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	//Method to print entire number
	
	public String toString() {
		return "Phone number: "+ countryCode +"-"+areaCode + "-" + number; 
	}
	
	//Method to check whether area code is 3 digits
	public boolean areaCodelen3() {
		if (areaCode.length()==3)
			return true;
		return false;
	}
	
	//Method to check whether the number is 7 digits
	
	public boolean numberLen7() {
		if(number.length()==7)
			return true;
		return false;
	}
	
}
