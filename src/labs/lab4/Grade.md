# Lab 4

## Total

27/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        7/8
  * Application Class   1/1
* Documentation         3/3

## Comments
For your GeoLocation class, you should put in default values for your default constructor. These can be any values. No points off but this does matter for future programming.

All your classes are missing the equals() method. -3 Points