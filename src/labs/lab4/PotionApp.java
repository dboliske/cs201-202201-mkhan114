package labs.lab4;

import java.util.Scanner;

public class PotionApp {

	public static void main(String[] args) {
		
		Potion potion1 = new Potion();
		
		System.out.println(potion1.toString());
		System.out.println();
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter a name for the potion: ");
		String name = input.next();
		System.out.println("Enter the strength for the potion: ");
		double strength = input.nextDouble();
		
		input.close();
		
		Potion potion2 = new Potion(name,strength);
		
		System.out.println();
		System.out.println(potion2.toString());
	}

}
